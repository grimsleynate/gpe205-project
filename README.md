#UATanks
UATanks is a game prototype created in Unity which includes examples of game logic and artificial intelligence.
##Installation
If you would like to try the game, download and run the included .exe file.
##Usage
This project contains a motor component that will make player and enemy tanks move, as well as an input logic script
for the player and an AI logic script for the enemy tanks. The AI logic script contains five different AI personalities
that you can choose from, with the ability to add more personalities if needed.
##Contributors
Nathaniel Grimsley