﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankData : MonoBehaviour
{
    public float forwardSpeed = 8.0f; //this is our move speed for the tanks
    public float backwardSpeed = 3.0f; //this is our backward speed for the tanks.
    public float turnSpeed = 180.0f; //this is the turn speed for the tanks
    public float shootSpeed = 1500f; //this is how much force we apply to the bullet.
    public float shootDelay = 0.4f; //this is the delay before the user can fire another bullet.
    public float bulletDamage = 1.0f; //this sets the amount of damage the tank will do per bullet.
    public float startHealth = 3.0f; //this sets the amount of life this tank has.
    public float maxHealth = 3.0f; //this sets the maximum amount of life a tank has.
    public float killPoints = 100.0f; //this is the amount of points this tank is worth for a kill.
    public float restingHealRate = 0.25f; //this is how quickly the enemy heals in hp / second.
    [HideInInspector] public float health; //this stores the current health of the tank.
    [HideInInspector] public float score; //this is the player's score.

    // Start is called before the first frame update
    void Start()
    {
        health = startHealth;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
