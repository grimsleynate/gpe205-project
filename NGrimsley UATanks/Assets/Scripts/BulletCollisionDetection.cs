﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCollisionDetection : MonoBehaviour
{
    [HideInInspector] public string origin;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // If this object touches any other object
    private void OnCollisionEnter(Collision collision)
    {
        //destroy this object.
        Destroy(gameObject);
    }
}
