﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public TankData data; //This pulls the tank data variables into this script
    public TankMotor motor; //this allows the InputController to move the tank.                                                                                          
    public enum InputScheme { WASD, arrowKeys }; //this allows the designer to choose the controller layout
    public InputScheme input = InputScheme.WASD; //we set the default controller scheme to WASD
    private float nextBullet; //This stores the length of time until we can fire a bullet again.

    // Start is called before the first frame update
    void Start()
    {
        nextBullet = Time.time; //we set nextBullet equal to the current time, which allows us to fire a bullet immediately
    }

    // Update is called once per frame
    void Update()
    {
        switch (input) //this switch case runs through input; each case is an input scheme
        {
            case InputScheme.arrowKeys: //if the input scheme is equal to arrowKeys
                if (Input.GetKey(KeyCode.UpArrow)) //if the user presses the up arrow
                {
                    motor.Move(data.forwardSpeed); //move the tank forward
                }
                if (Input.GetKey(KeyCode.DownArrow)) //if the user presses the down arrow
                {
                    motor.Move(-data.backwardSpeed); //move the tank down
                }
                if (Input.GetKey(KeyCode.RightArrow)) //if the user presses the right arrow
                {
                    motor.Rotate(data.turnSpeed); //move the tank right
                }
                if (Input.GetKey(KeyCode.LeftArrow)) // if the user presses the left arrow
                {
                    motor.Rotate(-data.turnSpeed); //move the tank left
                }
                if (Input.GetKey(KeyCode.Space)) //if the user presses space
                {
                    if (Time.time >= nextBullet) // and the bullet delay is over
                    {
                        motor.Shoot(data.shootSpeed); //shoot a bullet from the tank
                        nextBullet = Time.time + data.shootDelay; //added our delay amount to the time and add it to the nextBullet variable.
                    }
                }
                break; //break out of the switch case.

            case InputScheme.WASD: // if InputScheme is equal to WASD
                if (Input.GetKey(KeyCode.W)) //if the user presses W
                {
                    motor.Move(data.forwardSpeed); //move the tank forward
                }
                if (Input.GetKey(KeyCode.S)) //if the user presses S
                {
                    motor.Move(-data.backwardSpeed); //move the tank backward
                }
                if (Input.GetKey(KeyCode.D)) //if the user presses D
                {
                    motor.Rotate(data.turnSpeed); //rotate the tank to the right
                }
                if (Input.GetKey(KeyCode.A)) //if the user presses A
                {
                    motor.Rotate(-data.turnSpeed); //rotate the user to the left.
                }
                if (Input.GetKey(KeyCode.Space)) //if the user presses space
                {
                    if (Time.time >= nextBullet) //and the bullet delay is over
                    {
                        motor.Shoot(data.shootSpeed); //shoot a bullet from the tank
                        nextBullet = Time.time + data.shootDelay; //add our delay amount to the time and ad it ot the nextBullet variable.
                    }
                }
                break; //break out of the switch case.
        }
    }
}
