﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public static GameManager instance;
    public GameObject player;
    [HideInInspector] public TankData playerTankData;

    // Awake is called before any Start() functions run
    void Awake()
    {
        // if our instance variable is empty
        if (instance == null)
        {
            //assign this GameManager to the instance variable
            instance = this;
        }
        //else if there is a GameManager assigned to instance
        else
        {
            //print to the console that there can only be one GameManager
            Debug.LogError("ERROR: There can only be one GameManager.");
            //destroy the gameObject that this GameManager is attached to
            Destroy(gameObject);
        }

        if (player == null)
        {
            player = GameObject.Find("Player 1");
        }

        playerTankData = player.GetComponent<TankData>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
