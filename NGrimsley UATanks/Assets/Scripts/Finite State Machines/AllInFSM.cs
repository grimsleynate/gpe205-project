﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//we are adding requirements for this object to have a TankMotor, TankData, and Transform component.
[RequireComponent(typeof(TankMotor))]
[RequireComponent(typeof(TankData))]
[RequireComponent(typeof(Transform))]

public class AllInFSM : MonoBehaviour
{
    public enum AIState { Chase, ChaseAndFire, CheckForFlee, Flee, Rest}; //an enum to hold our states
    public AIState aiState = AIState.Chase; //we initially set the steate to Chase
    public float stateEnterTime; //we create a variable to store when we entered a state
    public float aiSenseRadius; //this allows the designers to set the sense radius for enemies.
    public Transform target; //this stores our target's Transform
    public float avoidanceTime = 2.0f; //this stores how long to stay in avoidance mode.
    public float fleeDistance = 1.0f; //how far we want to flee from the target.
    public float fleeTime = 10.0f; //this stores how long we want to flee for.
    public int fieldOfViewRange = 70; //this stores the field of view. (double the number for the full field of view; 70 is equivalent to 140 degree full fiew.
    public float lengthOfViewRange = 5.0f; //this stores how far in front of him the tank can see.
    public Transform tf; //this stores this object's tank data
    private TankMotor motor; //this stores this object's tank motor
    private TankData data; //this stores this object's tank data
    private float exitTime; //this stores the time until we can exit the state.
    private float shootTime; //this stores the time until we can shoot again.
    private Vector3 rayDirection; //this will store the players position minus the tank position
    


    // Start is called before the first frame update
    void Start()
    {
        //if the designer did not designate a target
        if (target == null)
        {
            target = GameManager.instance.player.GetComponent<Transform>();
        }

        //we assign this player's TankMotor to the motor variable.
        motor = gameObject.GetComponent<TankMotor>();
        //we assign this player's TankData to the data variable.
        data = gameObject.GetComponent<TankData>();
        //we assign this player's Transform to the tf variable.
        tf = gameObject.GetComponent<Transform>();
        //we set shootTime equal to Time.time 
        shootTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        //we run a switch case depending on the state we are in.
        switch (aiState)
        {
            //if we are in the Chase state,
            case AIState.Chase:
                //and if we have an obstacle in front of us
                if (motor.avoidanceStage != 0)
                {
                    //we run AvoidObstacle to avoid obstacles
                    motor.AvoidObstacle();
                }
                //otherwise if there is not an obstacle in front of us,
                else
                {
                    //we chase the target
                    Chase();
                }

                //Next we check for transitions.
                //if our health is less than half of max health,
                if (data.health <= data.maxHealth * 0.5f)
                {
                    //we change to the CheckForFlee state.
                    ChangeState(AIState.CheckForFlee);
                }
                //otherwise, if the player is within the aiSenseRadius
                else if ((target.position - tf.position).sqrMagnitude <= aiSenseRadius * aiSenseRadius)
                {
                    //we change to the chase and fire state
                    ChangeState(AIState.ChaseAndFire);
                }
                //we break from this case
                break;

            //if we are in the ChaseAndFire state,
            case AIState.ChaseAndFire:
                //and if an obstacle is in front of us
                if (motor.avoidanceStage != 0)
                {
                    //we run AvoidObstacle to Avoid obstacles.
                    motor.AvoidObstacle();
                }
                //otherwise if there is not an obstacle in front of us,
                else
                {
                    //we chase the target
                    Chase();

                    //we check to make sure enough time has passed between bullets
                    if (Time.time > shootTime + data.shootDelay)
                    {
                        //shoot a shell
                        motor.Shoot(data.shootSpeed);
                        //set our shootTime back to the current time.
                        shootTime = Time.time;
                    }
                }

                //now we check for transitions
                //if the tank's health is less than or equal to half of its max health,
                if (data.health <= data.maxHealth * 0.5)
                {
                    //we ChangeState() to CheckForFlee.
                    ChangeState(AIState.CheckForFlee);
                }
                //otherwise if the player is not in range of this tank's senses,
                else if ((target.position - tf.position).sqrMagnitude <= aiSenseRadius * aiSenseRadius)
                {
                    //we ChangeState() to Chase.
                    ChangeState(AIState.Chase);
                }
                //we break from this case.
                break;

            //if we are in the flee state,
            case AIState.Flee:
                //Perform Behaviors
                //if there is an obstacle to avoid,
                if (motor.avoidanceStage != 0)
                {
                    //we run the AvoidObstacle function
                    motor.AvoidObstacle();
                }
                //otherwise if there is no obstacle to avoid,
                else
                {
                    //we run the Flee function.
                    Flee();
                }

                //Check for Transitions
                //if we have been in the Flee state for the fleeTime seconds,
                if (Time.time >= stateEnterTime + fleeTime)
                {
                    //we change to the CheckForFlee state
                    ChangeState(AIState.CheckForFlee);
                }
                //we break from this case.
                break;

            //if we are in the CheckForFlee state,
            case AIState.CheckForFlee:
                //Perform Behaviors
                //We run CheckForFlee
                CheckForFlee();

                //Check for Transitions
                //if the target is within the ai's sense radius,
                if ((target.position - tf.position).sqrMagnitude <= aiSenseRadius * aiSenseRadius)
                {
                    //we change the state to flee
                    ChangeState(AIState.Flee);
                }
                //otherwise if the player is not in the ai's sense radius,
                else
                {
                    //we change to the Rest state.
                    ChangeState(AIState.Rest);
                }
                //we break from this case.
                break;

            //if we are in the Rest state
            case AIState.Rest:
                //Perform Behaviors
                //We run the Rest function.
                Rest();

                //Check for Transitions
                //if the target is within the ai's sense radius,
                if ((target.position - tf.position).sqrMagnitude <= aiSenseRadius * aiSenseRadius)
                {
                    //we change to the flee state
                    ChangeState(AIState.Flee);
                }
                //otherwise if the tank's health is full,
                else if (data.health >= data.maxHealth)
                {
                    //we change to the Chase state.
                    ChangeState(AIState.Chase);
                }
                //we break from this case.
                break;
        }
    }

    // This is used to chase the player
    void Chase()
    {
        //we rotate the tank to look at the player
        motor.RotateTowards(target.position, data.turnSpeed);
        //check to see if we can move forwardSpeed units away.
        if (motor.CanMove(data.forwardSpeed))
        {
            //move forward
            motor.Move(data.forwardSpeed);
        }
        //otherwise if we can't move forwardSpeed units away.
        else
        {
            //start avoiding obstacles stage one.
            motor.avoidanceStage = 1;
        }
    }

   
    void Flee()
    {
        // we set a new vector, vectorToTarget, equal to the target position minus our position.
        Vector3 vectorToTarget = target.position - tf.position;

        //we flip the vector so it faces away from the target.
        Vector3 vectorAwayFromTarget = vectorToTarget * -1;

        //we normalize the Vector3 so it has a length of 1 unit.
        vectorAwayFromTarget.Normalize();

        //we mutliply our normalized vector by the distance the designer wishes this tank to flee.
        vectorAwayFromTarget *= fleeDistance;

        //we add the new vector3 to the AI's current position, moving it away from the target until it is fleeDistance away from it.
        Vector3 fleePosition = vectorAwayFromTarget + tf.position;
        //we rotate our tank away from the target
        motor.RotateTowards(fleePosition, data.turnSpeed);
        //we move the tank away from the target.
        motor.Move(data.forwardSpeed);
    }

    void CheckForFlee()
    {
        // TODO: Write the CheckForFlee state.
    }

    void Rest()
    {
        //Increase our health and change it to per second instead of per frame.
        data.health += data.restingHealRate * Time.deltaTime;

        //But never go over our max health. Mathf.Min will make our data.health variable
        //the smallest number between our max health and current health. this means if we healed 
        //more than our max health, our health will be set to max health.
        data.health = Mathf.Min(data.health, data.maxHealth);
        Debug.Log(gameObject.name + "'s health: " + data.health);
    }

    bool CanSeePlayer()
    {
        //we create a new RaycastHit
        RaycastHit hit;
        //we set rayDirection equal to the offset between the player and the tank.
        rayDirection = target.position - tf.position;
        //we create a variable to store the distance between the player and this tank
        float distanceToPlayer = Vector3.Distance(tf.position, target.position);

        //if the player is within the field of view,
        if(Vector3.Angle(rayDirection, tf.forward) < fieldOfViewRange)
        {
            //and the object is close enough to see,
            if (Physics.Raycast(tf.position, rayDirection, out hit, lengthOfViewRange))
            {
                //and if the object has the "Player" tag
                if (hit.transform.tag == "Player")
                {
                    Debug.Log("I can see the player.");
                    //return true
                    return true;
                }
                //otherwise if what it sees is not the player
                else
                {
                    Debug.Log("Can not see the player.");
                    //return false.
                    return false;
                }
            }
        }
        //if none of this happens, return false.
        return false;
    }

    

  
    void ChangeState (AIState newState)
    {
        //Change our state
        aiState = newState;
        Debug.Log(newState);
        //save the time we changed the state.
        stateEnterTime = Time.time;
    }

    //we use this function to draw gizmos when the object is selected in the inspector.
    private void OnDrawGizmosSelected()
    {
        //we store our total field of view in a variable.
        float totalFOV = fieldOfViewRange * 2;
        //we set up the rotations needed for our rays
        Quaternion leftRayRotation = Quaternion.AngleAxis(-fieldOfViewRange, Vector3.up);
        Quaternion rightRayRotation = Quaternion.AngleAxis(fieldOfViewRange, Vector3.up);
        //we set up vector3s looking forward at the angles we set up.
        Vector3 leftRayDirection = leftRayRotation * tf.forward;
        Vector3 rightRayDirection = rightRayRotation * tf.forward;

        //we choose a color for our first gizmo (line of sight)
        Gizmos.color = Color.cyan;
        //we draw a ray using this color in front of the player
        Gizmos.DrawRay(tf.position, leftRayDirection * lengthOfViewRange);
        Gizmos.DrawRay(tf.position, rightRayDirection * lengthOfViewRange);
        //we choose a color for our second gizmo (hearing)
        Gizmos.color = Color.magenta;
        //we draw a circle around the tank equal to where he can hear.
        Gizmos.DrawWireSphere(tf.position, aiSenseRadius);
    }
}