﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoicFSM : MonoBehaviour
{
    public enum AIState {ChaseAndFire, Patrol, HoldYourGround }; //an enum to hold our states
    public AIState aiState = AIState.Patrol; //we initially set the steate to Chase
    public enum LoopType { Stop, Loop, PingPong }; //this stores our types of loops
    public LoopType loopType = LoopType.Stop; //this allows the designers to choose a loop type in the inspector.
    public Transform[] waypoints; //this allows the designers to add transforms into an array of transforms.
    public float closeEnoughToWaypoint = 2.0f; //this allows the designer to choose how close is close enough to a waypoint.
    public float stateEnterTime; //we create a variable to store when we entered a state
    public float aiSenseRadius; //this allows the designers to set the sense radius for enemies.
    public Transform target; //this stores our target's Transform
    public float avoidanceTime = 2.0f; //this stores how long to stay in avoidance mode.
    public int fieldOfViewRange = 70; //this stores the field of view. (double the number for the full field of view; 70 is equivalent to 140 degree full fiew.
    public float lengthOfViewRange = 5.0f; //this stores how far in front of him the tank can see.
    public Transform tf; //this stores this object's tank data
    private TankMotor motor; //this stores this object's tank motor
    private TankData data; //this stores this object's tank data
    private float shootTime; //this stores the time until we can shoot again.
    private Vector3 rayDirection; //this will store the players position minus the tank position
    private int currentWaypoint = 0; //this sets our current waypoint to waypoint 0.
    private bool isForward; //this will tell our PingPong function which way to travel.

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        switch (aiState)
        {
            case AIState.Patrol:
                //Perform behaviors.
                Patrol();
                //Heal the tank while he is patrolling.
                Rest();

                //Check for Transitions
                //if the player is in range
                if ((target.position - tf.position).sqrMagnitude <= aiSenseRadius * aiSenseRadius || CanSeePlayer())
                {
                    //we switch state to Chase and Fire
                    ChangeState(AIState.ChaseAndFire);
                }
                break;
            case AIState.ChaseAndFire:
                //Perform behaviors
                //and if an obstacle is in front of us
                if (motor.avoidanceStage != 0)
                {
                    //we run AvoidObstacle to Avoid obstacles.
                    motor.AvoidObstacle();
                }
                //otherwise if there is not an obstacle in front of us,
                else
                {
                    //we chase the target
                    Chase();

                    //we check to make sure enough time has passed between bullets
                    if (Time.time > shootTime + data.shootDelay)
                    {
                        //shoot a shell
                        motor.Shoot(data.shootSpeed);
                        //set our shootTime back to the current time.
                        shootTime = Time.time;
                    }
                }

                //now we check for transitions
                //if the tank's health is less than or equal to half of its max health,
                if (data.health <= data.maxHealth * 0.5)
                {
                    //we change state to HoldYourGround
                    ChangeState(AIState.HoldYourGround);
                }
                //otherwise if the player is not in range of this tank's senses,
                else if ((target.position - tf.position).sqrMagnitude >= aiSenseRadius * aiSenseRadius && !CanSeePlayer())
                {
                    //we ChangeState() to Patrol.
                    ChangeState(AIState.Patrol);
                }
                //we break from this case.
                break;
            case AIState.HoldYourGround:
                //perform behaviors
                //we check to make sure enough time has passed between bullets. we divide our shootDelay
                //by half as part of the modifications of this state.
                if (Time.time > shootTime + (data.shootDelay / 2))
                {
                    //shoot a shell
                    motor.Shoot(data.shootSpeed);
                    //set our shootTime back to the current time.
                    shootTime = Time.time;
                }

                //we check for transitions
                //if the target is not in this tank's senses
                if ((target.position - tf.position).sqrMagnitude >= aiSenseRadius * aiSenseRadius && !CanSeePlayer())
                {
                    //we change back to the Patrol state.
                    ChangeState(AIState.Patrol);
                }
                break;
        }
    }

    void Patrol()
    {
        //if the tank can continue to rotate towards the current waypoint, do so.
        if (motor.RotateTowards(waypoints[currentWaypoint].position, data.turnSpeed))
        {
            // Do nothing!
        }
        //otherwise if it is lined up with the next waypoint
        else
        {
            // Move Forward
            motor.Move(data.forwardSpeed);
        }

        //if the distance between this object and the current waypoint is less than closeEnough
        if (Vector3.SqrMagnitude(waypoints[currentWaypoint].position - tf.position) < (closeEnoughToWaypoint * closeEnoughToWaypoint))
        {
            //if the designer chose Stop type
            if (loopType == LoopType.Stop)
            {
                //run LoopStop() function.
                LoopStop();
            }
            //otherwise if the designer chose loop type
            else if (loopType == LoopType.Loop)
            {
                LoopLoop();
            }
            //otherwise if the designer chose the ping pong type
            else if (loopType == LoopType.PingPong)
            {
                LoopPingPong();
            }
        }
    }

    void Rest()
    {
        //Increase our health and change it to per second instead of per frame.
        data.health += data.restingHealRate * Time.deltaTime;

        //But never go over our max health. Mathf.Min will make our data.health variable
        //the smallest number between our max health and current health. this means if we healed 
        //more than our max health, our health will be set to max health.
        data.health = Mathf.Min(data.health, data.maxHealth);
        Debug.Log(gameObject.name + "'s health: " + data.health);
    }

    bool CanSeePlayer()
    {
        //we create a new RaycastHit
        RaycastHit hit;
        //we set rayDirection equal to the offset between the player and the tank.
        rayDirection = target.position - tf.position;
        //we create a variable to store the distance between the player and this tank
        float distanceToPlayer = Vector3.Distance(tf.position, target.position);

        //if the player is within the field of view,
        if (Vector3.Angle(rayDirection, tf.forward) < fieldOfViewRange)
        {
            //and the object is close enough to see,
            if (Physics.Raycast(tf.position, rayDirection, out hit, lengthOfViewRange))
            {
                //and if the object has the "Player" tag
                if (hit.transform.tag == "Player")
                {
                    Debug.Log("I can see the player.");
                    //return true
                    return true;
                }
                //otherwise if what it sees is not the player
                else
                {
                    Debug.Log("Can not see the player.");
                    //return false.
                    return false;
                }
            }
        }
        //if none of this happens, return false.
        return false;
    }

    void ChangeState(AIState newState)
    {
        //Change our state
        aiState = newState;
        Debug.Log(newState);
        //save the time we changed the state.
        stateEnterTime = Time.time;
    }

    //we use this function to draw gizmos when the object is selected in the inspector.
    private void OnDrawGizmosSelected()
    {
        //we store our total field of view in a variable.
        float totalFOV = fieldOfViewRange * 2;
        //we set up the rotations needed for our rays
        Quaternion leftRayRotation = Quaternion.AngleAxis(-fieldOfViewRange, Vector3.up);
        Quaternion rightRayRotation = Quaternion.AngleAxis(fieldOfViewRange, Vector3.up);
        //we set up vector3s looking forward at the angles we set up.
        Vector3 leftRayDirection = leftRayRotation * tf.forward;
        Vector3 rightRayDirection = rightRayRotation * tf.forward;

        //we choose a color for our first gizmo (line of sight)
        Gizmos.color = Color.cyan;
        //we draw a ray using this color in front of the player
        Gizmos.DrawRay(tf.position, leftRayDirection * lengthOfViewRange);
        Gizmos.DrawRay(tf.position, rightRayDirection * lengthOfViewRange);
        //we choose a color for our second gizmo (hearing)
        Gizmos.color = Color.magenta;
        //we draw a circle around the tank equal to where he can hear.
        Gizmos.DrawWireSphere(tf.position, aiSenseRadius);
    }

    //LoopStop() tells the tank to go to the next waypoint if there is one, and to stop if there is not.
    private void LoopStop()
    {
        if (currentWaypoint < waypoints.Length - 1)
        {
            //Advance to the next waypoint
            currentWaypoint++;
        }
    }

    private void LoopLoop()
    {
        if (currentWaypoint < waypoints.Length - 1)
        {
            currentWaypoint++;
        }
        else
        {
            currentWaypoint = 0;
        }
    }

    private void LoopPingPong()
    {
        //if isForward is true,
        if (isForward)
        {
            //and if we aren't at the last waypoint
            if (currentWaypoint < waypoints.Length - 1)
            {
                //move to the next waypoint.
                currentWaypoint++;
            }
            //otherwise if we are at the last waypoint
            else
            {
                //change isForward to false and decrement the list of waypoints.
                isForward = false;
                currentWaypoint--;
            }
        }
        //otherwise if isForward is false,
        else
        {
            //and we aren't at the first waypoint
            if (currentWaypoint > 0)
            {
                //move to the next waypoint down the list.
                currentWaypoint--;
            }
            //otherwise if we are at the first waypoint
            else
            {
                //change isForward to true and increment the list of waypoints.
                isForward = true;
                currentWaypoint++;
            }
        }
    }

    // This is used to chase the player
    void Chase()
    {
        //we rotate the tank to look at the player
        motor.RotateTowards(target.position, data.turnSpeed);
        //check to see if we can move forwardSpeed units away.
        if (motor.CanMove(data.forwardSpeed))
        {
            //move forward
            motor.Move(data.forwardSpeed);
        }
        //otherwise if we can't move forwardSpeed units away.
        else
        {
            //start avoiding obstacles stage one.
            motor.avoidanceStage = 1;
        }
    }
}
