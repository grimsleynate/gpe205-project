﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//we are adding requirements for this object to have a TankMotor, TankData, and Transform component.
[RequireComponent(typeof(TankMotor))]
[RequireComponent(typeof(TankData))]
[RequireComponent(typeof(Transform))]

public class TurretFSM : MonoBehaviour
{
    public enum AIState { Passive, ShieldsUp, Shoot }; //an enum to hold our states
    public AIState aiState = AIState.Passive; //we initially set the steate to Chase
    public float stateEnterTime; //we create a variable to store when we entered a state
    public float aiSenseRadius; //this allows the designers to set the sense radius for enemies.
    public Transform target; //this stores our target's Transform
    public int fieldOfViewRange = 70; //this stores the field of view. (double the number for the full field of view; 70 is equivalent to 140 degree full fiew.
    public float lengthOfViewRange = 5.0f; //this stores how far in front of him the tank can see.
    public Transform tf; //this stores this object's tank data
    public float turretRotation = 50.0f; //this stores how far the turret will rotate in one direction before turning.
    public GameObject shield; //this stores the shield prefab for our turret personality
    private TankMotor motor; //this stores this object's tank motor
    private TankData data; //this stores this object's tank data
    private float exitTime; //this stores the time until we can exit the state.
    private float shootTime; //this stores the time until we can shoot again.
    private Vector3 rayDirection; //this will store the players position minus the tank position
    private bool shieldsUp = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        switch (aiState)
        {
            case AIState.Passive:
                //we perform behaviors
                //we rotate the turret
                tf.Rotate(Vector3.up * data.turnSpeed * Time.deltaTime);

                //we check for transitions
                //if the health is below half
                if (data.health <= data.maxHealth * 0.5f)
                {
                    //transition to ShieldsUp
                    ChangeState(AIState.ShieldsUp);
                }
                //if we can see the target or hear the target
                if (CanSeePlayer() || (target.position - tf.position).sqrMagnitude <= aiSenseRadius * aiSenseRadius)
                {
                    //transition to Shoot
                    ChangeState(AIState.Shoot);
                }
                break;
            case AIState.ShieldsUp:
                //we perform behaviors
                //we rotate the tank to look at the player
                motor.RotateTowards(target.position, data.turnSpeed);
                //if shieldsUp is false,
                if (!shieldsUp)
                {
                    //we instantiate a shield
                    CreateShield();
                    //and switch shieldsUp to true
                    shieldsUp = true;
                    //we only want this tank to create one shield right now, so this is where we stop.
                }
                //we create a timer for five seconds
                if (Time.time >= stateEnterTime + 5.0f)
                {
                    //when the timer is up, start healing
                    Rest();
                }

                //we check for transitions
                //if hp is full
                if (data.health >= data.maxHealth)
                {
                    //transition to Passive
                    ChangeState(AIState.Passive);
                }
                break;

            case AIState.Shoot:
                //we perform behaviors
                //we rotate to look at the player
                motor.RotateTowards(target.position, data.turnSpeed * 2.0f);
                //we check to make sure enough time has passed between bullets
                if (Time.time > shootTime + data.shootDelay)
                {
                    //shoot a shell
                    motor.Shoot(data.shootSpeed);
                    //set our shootTime back to the current time.
                    shootTime = Time.time;
                }

                //we check for transitions
                //if health is below half
                if (data.health <= data.maxHealth * 0.5f)
                {
                    //switch state to ShieldsUp
                    ChangeState(AIState.ShieldsUp);
                }
                //if we can't see the player
                if (!CanSeePlayer() && (target.position - tf.position).sqrMagnitude >= aiSenseRadius * aiSenseRadius)
                {
                    //switch state to Passive
                    ChangeState(AIState.Passive);
                }
                break;
        }
    }

    void CreateShield()
    {
        //This creates a new GameObject in the game that is a copy of the shield.
        GameObject shieldInstance = Instantiate(shield, tf.position, tf.rotation) as GameObject;
    }

    void Rest()
    {
        //Increase our health and change it to per second instead of per frame.
        data.health += data.restingHealRate * Time.deltaTime;

        //But never go over our max health. Mathf.Min will make our data.health variable
        //the smallest number between our max health and current health. this means if we healed 
        //more than our max health, our health will be set to max health.
        data.health = Mathf.Min(data.health, data.maxHealth);
        Debug.Log(gameObject.name + "'s health: " + data.health);
    }

    void ChangeState(AIState newState)
    {
        //Change our state
        aiState = newState;
        Debug.Log(newState);
        //save the time we changed the state.
        stateEnterTime = Time.time;
    }

    //we use this function to draw gizmos when the object is selected in the inspector.
    private void OnDrawGizmosSelected()
    {
        //we store our total field of view in a variable.
        float totalFOV = fieldOfViewRange * 2;
        //we set up the rotations needed for our rays
        Quaternion leftRayRotation = Quaternion.AngleAxis(-fieldOfViewRange, Vector3.up);
        Quaternion rightRayRotation = Quaternion.AngleAxis(fieldOfViewRange, Vector3.up);
        //we set up vector3s looking forward at the angles we set up.
        Vector3 leftRayDirection = leftRayRotation * tf.forward;
        Vector3 rightRayDirection = rightRayRotation * tf.forward;

        //we choose a color for our first gizmo (line of sight)
        Gizmos.color = Color.cyan;
        //we draw a ray using this color in front of the player
        Gizmos.DrawRay(tf.position, leftRayDirection * lengthOfViewRange);
        Gizmos.DrawRay(tf.position, rightRayDirection * lengthOfViewRange);
        //we choose a color for our second gizmo (hearing)
        Gizmos.color = Color.magenta;
        //we draw a circle around the tank equal to where he can hear.
        Gizmos.DrawWireSphere(tf.position, aiSenseRadius);
    }

    bool CanSeePlayer()
    {
        //we create a new RaycastHit
        RaycastHit hit;
        //we set rayDirection equal to the offset between the player and the tank.
        rayDirection = target.position - tf.position;
        //we create a variable to store the distance between the player and this tank
        float distanceToPlayer = Vector3.Distance(tf.position, target.position);

        //if the player is within the field of view,
        if (Vector3.Angle(rayDirection, tf.forward) < fieldOfViewRange)
        {
            //and the object is close enough to see,
            if (Physics.Raycast(tf.position, rayDirection, out hit, lengthOfViewRange))
            {
                //and if the object has the "Player" tag
                if (hit.transform.tag == "Player")
                {
                    Debug.Log("I can see the player.");
                    //return true
                    return true;
                }
                //otherwise if what it sees is not the player
                else
                {
                    Debug.Log("Can not see the player.");
                    //return false.
                    return false;
                }
            }
        }
        //if none of this happens, return false.
        return false;
    }
}
