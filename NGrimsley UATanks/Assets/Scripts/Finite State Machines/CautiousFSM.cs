﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CautiousFSM : MonoBehaviour
{

    public enum AIState { Passive, BackPedalAndFire, Flee };
    public AIState aiState = AIState.Passive;
    public Transform target;
    public Transform tf;
    public float aiSenseRadius = 2.0f;
    public float stateEnterTime; //we create a variable to store when we entered a state
    public float fleeDistance = 1.0f; //how far we want to flee from the target.
    public int fieldOfViewRange = 70; //this stores the field of view. (double the number for the full field of view; 70 is equivalent to 140 degree full fiew.
    public float lengthOfViewRange = 5.0f; //this stores how far in front of him the tank can see.
    private TankMotor motor;
    private TankData data;
    private MeshRenderer[] mesh; //this stores this game object's mesh as well as its children's meshes.
    private float shootTime; //this stores the time until we can shoot again.

    // Start is called before the first frame update
    void Start()
    {
        motor = gameObject.GetComponent<TankMotor>();
        data = gameObject.GetComponent<TankData>();
        tf = gameObject.GetComponent<Transform>();
        //we find the mesh on this object or its children
        mesh = GetComponentsInChildren<MeshRenderer>();

        if (target == null)
        {
            target = GameManager.instance.player.GetComponent<Transform>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        switch (aiState)
        {
            case AIState.Passive:
                //we perform behaviors
                //we rotate the tank to look at the player always.
                motor.RotateTowards(target.position, data.turnSpeed);

                //we check for transitions.
                //if the player is in the sense area
                if ((target.position - tf.position).sqrMagnitude <= aiSenseRadius * aiSenseRadius)
                {
                    //we switch to shoot and back pedal
                    ChangeState(AIState.BackPedalAndFire);
                }

                //or if the tank's hp is lower than half
                if (data.health <= data.maxHealth * 0.5)
                {
                    //we change to the Flee state
                    ChangeState(AIState.Flee);
                }
                break;
            case AIState.BackPedalAndFire:
                //and if an obstacle is in front of us
                if (motor.avoidanceStage != 0)
                {
                    //we run AvoidObstacle to Avoid obstacles.
                    motor.AvoidObstacle();
                }
                //otherwise if there is not an obstacle in front of us,
                else
                {
                    //if the player is in the sense area
                    if ((target.position - tf.position).sqrMagnitude <= aiSenseRadius * aiSenseRadius)
                    {
                        //we run away from the target.
                        BackPedal();
                    }
                    //otherwise if the player isn't in the sense area any more,
                    else
                    {
                        //we chase the target.
                        Chase();
                    }

                    //we check to make sure enough time has passed between bullets
                    if (Time.time > shootTime + data.shootDelay)
                    {
                        //shoot a shell
                        motor.Shoot(data.shootSpeed);
                        //set our shootTime back to the current time.
                        shootTime = Time.time;
                    }
                }

                //we check for transitions
                //if hp is less than half
                if (data.health <= data.maxHealth * 0.5)
                {
                    //we change to the flee state
                    ChangeState(AIState.Flee);
                }
                break;
            case AIState.Flee:
                //we run behaviors
                //if the player is within 1.5 * aiSenseRadius
                if ((target.position - tf.position).sqrMagnitude <= (aiSenseRadius * aiSenseRadius) * 1.5)
                {
                    //we flee
                    Flee();
                    //we iterate through the mesh array to disable every mesh on this object.
                    for (int i = 0; i < mesh.Length; i++)
                    {
                        mesh[i].enabled = false;
                    }
                }
                else
                {
                    //we iterate through the mesh array to enable every mesh on this object.
                    for (int i = 0; i < mesh.Length; i++)
                    {
                        mesh[i].enabled = true;
                    }
                    Rest();
                }
                //we check for transitions
                //if hp is max
                if (data.health >= data.maxHealth)
                {
                    //change state back to passive
                    ChangeState(AIState.Passive);
                }
                break;
        }
    }

    void BackPedal()
    {
        //we rotate towards the player
        motor.RotateTowards(target.position, data.turnSpeed);
        //check to see if we can move forwardSpeed units away.
        if (motor.CanMove(data.forwardSpeed))
        {
            //move backward
            motor.Move(-(data.forwardSpeed));
        }
        //otherwise if we can't move forwardSpeed units away.
        else
        {
            //start avoiding obstacles stage one.
            motor.avoidanceStage = 1;
        }
    }

    void Rest()
    {
        //Increase our health and change it to per second instead of per frame.
        data.health += data.restingHealRate * Time.deltaTime;

        //But never go over our max health. Mathf.Min will make our data.health variable
        //the smallest number between our max health and current health. this means if we healed 
        //more than our max health, our health will be set to max health.
        data.health = Mathf.Min(data.health, data.maxHealth);
        Debug.Log(gameObject.name + "'s health: " + data.health);
    }

    void Flee()
    {
        // we set a new vector, vectorToTarget, equal to the target position minus our position.
        Vector3 vectorToTarget = target.position - tf.position;

        //we flip the vector so it faces away from the target.
        Vector3 vectorAwayFromTarget = vectorToTarget * -1;

        //we normalize the Vector3 so it has a length of 1 unit.
        vectorAwayFromTarget.Normalize();

        //we mutliply our normalized vector by the distance the designer wishes this tank to flee.
        vectorAwayFromTarget *= fleeDistance;

        //we add the new vector3 to the AI's current position, moving it away from the target until it is fleeDistance away from it.
        Vector3 fleePosition = vectorAwayFromTarget + tf.position;
        //we rotate our tank away from the target
        motor.RotateTowards(fleePosition, data.turnSpeed);
        //we move the tank away from the target.
        motor.Move(data.forwardSpeed);
    }

    // This is used to chase the player
    void Chase()
    {
        //we rotate the tank to look at the player
        motor.RotateTowards(target.position, data.turnSpeed);
        //check to see if we can move forwardSpeed units away.
        if (motor.CanMove(data.forwardSpeed))
        {
            //move forward
            motor.Move(data.forwardSpeed);
        }
        //otherwise if we can't move forwardSpeed units away.
        else
        {
            //start avoiding obstacles stage one.
            motor.avoidanceStage = 1;
        }
    }

    void ChangeState(AIState newState)
    {
        //Change our state
        aiState = newState;
        Debug.Log(newState);
        //save the time we changed the state.
        stateEnterTime = Time.time;
    }

    //we use this function to draw gizmos when the object is selected in the inspector.
    private void OnDrawGizmosSelected()
    {
        //we store our total field of view in a variable.
        float totalFOV = fieldOfViewRange * 2;
        //we set up the rotations needed for our rays
        Quaternion leftRayRotation = Quaternion.AngleAxis(-fieldOfViewRange, Vector3.up);
        Quaternion rightRayRotation = Quaternion.AngleAxis(fieldOfViewRange, Vector3.up);
        //we set up vector3s looking forward at the angles we set up.
        Vector3 leftRayDirection = leftRayRotation * tf.forward;
        Vector3 rightRayDirection = rightRayRotation * tf.forward;

        //we choose a color for our first gizmo (line of sight)
        Gizmos.color = Color.cyan;
        //we draw a ray using this color in front of the player
        Gizmos.DrawRay(tf.position, leftRayDirection * lengthOfViewRange);
        Gizmos.DrawRay(tf.position, rightRayDirection * lengthOfViewRange);
        //we choose a color for our second gizmo (hearing)
        Gizmos.color = Color.magenta;
        //we draw a circle around the tank equal to where he can hear.
        Gizmos.DrawWireSphere(tf.position, aiSenseRadius);
    }
}
