﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
    public float shieldHealth = 3.0f; //this stores the shield's health

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //when something collides with this object
    private void OnCollisionEnter(Collision collision)
    {
        //if that object is a player's bullet
        if (collision.gameObject.name == "Bullet(Clone)")
        {
            //store the name of the tank that shot the bullet
            string bulletOrigin = collision.gameObject.GetComponent<BulletCollisionDetection>().origin;
            //store how much damage that shot does according to that tank's TankData
            float damage = GameObject.Find(bulletOrigin).GetComponent<TankData>().bulletDamage;
            //reduce the tank by the amount of damage the tank inflicts
            shieldHealth -= damage;
            //destroy the bullet
            Destroy(collision.gameObject);

            // if the shield has no life left,
            if (shieldHealth <= 0)
            {
                //destroy the shield.
                Destroy(gameObject);
            }
        }
    }
}
