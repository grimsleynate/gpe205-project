﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour
{

    public TankMotor motor; //this is the game object's TankMotor component.
    public TankData data; //this is the game object's TankData component.
    private float nextBullet; //this variable stores when we can fire our next bullet.
    
    // Start is called before the first frame update
    void Start()
    {
        nextBullet = Time.time; //we set nextBullet to the current time, allowing them to fire when the game starts.
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time >= nextBullet) //if the current time is equal to or greater than nextBullet
        {
            motor.Shoot(data.shootSpeed); //shoot a bullet.
            nextBullet = Time.time + data.shootDelay; //we can fire our next bullet after the current time plus our delay time.
        }
    }
}
