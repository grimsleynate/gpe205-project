﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//this ensures that this GameObject has a CharacterController attached to it.
[RequireComponent(typeof(CharacterController))]
public class TankMotor : MonoBehaviour
{
    // This is a private variable of CharacterController class named characterController.
    private CharacterController characterController;
    // This variable stores the Transform component of this game object instead of
    // constantly calling getComponent.
    public Transform tf;
    //this stores our bullet as a GameObject
    public GameObject shell;
    //this stores the transform of the cannon the bullet fires from.
    public Transform cannon;
    //this stores the TankData of this gameObject;
    public TankData data;
    [HideInInspector] public int avoidanceStage = 0; //this stores which stage of avoidance our tank is in.
    public float avoidanceTime = 2.0f; //this stores how long to stay in avoidance mode.
    private float exitTime; //this stores the time until we can exit the state.


    // Start is called before the first frame update
    void Start()
    {
        //we store the CharacterController attached to this GameObject.
        characterController = gameObject.GetComponent<CharacterController>();
        //if the user doesn't choose the GameObject to mess with transform
        if (tf == null)
        {
            //we set tf equal to this GameObject's transform component
            tf = gameObject.GetComponent<Transform>();
        }
        //if nothing is assigned to the data variable
        if (data == null)
        {
            //assign this game object's TankData to the data variable.
            data = gameObject.GetComponent<TankData>();
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    // This Move() function moves our GameObject forward.
    public void Move(float speed)
    {
        //this vector is going to store how fast the tank is going
        //we start by instantiating the variable and setting it equal to 
        //the direction the GameObject is looking.
        Vector3 speedVector = tf.forward;

        //we change the value of the speedVector according to the inputted speed value
        speedVector *= speed;

        //now we call the SimpleMove function of the characterController.
        //This will move the GameObject forward in meters per second.
        characterController.SimpleMove(speedVector);
    }

    // This Rotate() function rotates our GameObject.
    // We pass in a float named speed, which will be passed every
    // time we use this function.
    public void Rotate( float speed )
    {
        //We create a vector to store our rotation. transform.Rotate requires a Vector3
        //We use rotate to rotate around the Y axis, or "Yaw" around the center
        Vector3 rotateVector = Vector3.up;

        //Now we adjust rotateVector with our speed variable
        rotateVector *= speed;

        //We then multiply rotateVector by Time.deltaTime. This switches 
        //the rotation change from per frame to per second, which is much more constant
        //than per frame.
        rotateVector *= Time.deltaTime;

        //now we call the Rotate function of transform. The first parameter is the
        //vector that dictates how much we rotate, and the second dictates how we rotate 
        //(either locally or relative to world space.)
        tf.Rotate(rotateVector, Space.Self);
    }

    //This Shoot() function shoots a projectile from this GameObject.
    //We pass in a float named speed, which will effect the speed
    //of the projectile.
    public void Shoot( float speed )
    {
        //This creates a new GameObject in the game that is a copy of the bullet.
        //shell is the object to be created, cannon.position + transform.forward creates it at the position of cannon plus
        // one on the Z axis, and cannon.rotation shoots it in the direction of the cannon.
        GameObject shellInstance = Instantiate(shell, cannon.position + transform.forward, cannon.rotation) as GameObject;
        //this stores the shell's collision detection so we can store this gameObject's name in a variable on that script.
        BulletCollisionDetection shellCollisionDetection = shellInstance.GetComponent<BulletCollisionDetection>();
        //we set the BulletCollisionDetection component's origin variable to the name of this game object.
        shellCollisionDetection.origin = gameObject.name;
        //this stores the bullet's Rigidbody.
        Rigidbody shellInstanceRigidbody = shellInstance.GetComponent<Rigidbody>();
        //add force to the bullet equal to the direction the cannon is facing times the speed we declared times deltaTime, which allows us to measure the bullet speed in meters per second
        shellInstanceRigidbody.velocity = cannon.forward * speed * Time.deltaTime;
        //Destroy the shell after two seconds
        Destroy(shellInstance, 2);
    }

    //when a game object enters this trigger
    private void OnTriggerEnter(Collider other)
    {
        //and the game object's name is Bullet(Clone) 
        if (other.gameObject.name == "Bullet(Clone)" || other.gameObject.name == "BulletEnemy(Clone)")
        {
            //reduce this gameObject's life by the bullet damage
            data.health -= data.bulletDamage;
            //if this tank's life is 0 or less
            if (data.health <= 0)
            {
                //we store the bullet's origin in a variable.
                string origin = other.GetComponent<BulletCollisionDetection>().origin;
                //we store the bullet origin's score in a variable.
                GameObject.Find(origin).GetComponent<TankData>().score += data.killPoints;
                //Then we print to console the name of the player and the amount of points.
                Debug.Log(origin + "'s Score: " + GameObject.Find(origin).GetComponent<TankData>().score);
                //destroy this game object
                Destroy(gameObject);
            }
        }
    }
    
    // RotateTowards (Target, speed) - rotates towards the target (if possible)
    public bool RotateTowards(Vector3 target, float speed) //if the tank can rotate, return true. otherwise return false.
    {
        Vector3 vectorToTarget; //this stores the difference between the game object's position and the waypoint position.

        //We set the vector equal to the difference between the gme object's position and the waypoint's position.
        //The target is the waypoint's position, and tf.position is the game object's position.
        vectorToTarget = target - tf.position;

        //We fine a Quaternion that looks down our vectorToTarget vector.
        Quaternion targetRotation = Quaternion.LookRotation(vectorToTarget);
        //if this game object's rotation equals the target rotation,
        if (tf.rotation == targetRotation)
        {
            //return false
            return false;
        }
        //We set our rotation equal to the target rotation, but we use Quaternion.RotateTowards
        //to dictate how fast to rotate to the new position.
        tf.rotation = Quaternion.RotateTowards(tf.rotation, targetRotation, speed * Time.deltaTime);
        
        //return true;
        return true;
    }

    public void AvoidObstacle()
    {
        //we create a switch case for our obstacle avoidance
        switch (avoidanceStage)
        {
            //if we are in avoidance stage 1 (avoid),
            case 1:
                //rotate backwards
                Rotate(-1 * data.turnSpeed);

                //if we can now move forward, 
                if (CanMove(data.forwardSpeed))
                {
                    //move to stage 2
                    avoidanceStage = 2;

                    //set the number of seconds we are in stage 2
                    exitTime = avoidanceTime;
                }

                //otherwise we'll do this again next turn.
                break;
            //if we are in avoidance stage 2 (move forward),
            case 2:
                //and we can move forward,
                if (CanMove(data.forwardSpeed))
                {
                    //we subtract from our timer
                    exitTime -= Time.deltaTime;
                    //we move forward
                    Move(data.forwardSpeed);

                    //if we have moved long enough
                    if (exitTime <= 0)
                    {
                        //we return to avoidance stage zero, which is chase.
                        avoidanceStage = 0;
                    }
                    //otherwise (meaning we can't move forward)
                    else
                    {
                        //return to stage one, avoidance.
                        avoidanceStage = 1;
                    }
                }
                //we break from this case.
                break;
        }
    }

    public bool CanMove(float speed)
    {
        //we create a RayCast in front of the tank that checks for obstacles.
        RaycastHit hit;

        //if our raycast hit,
        if (Physics.Raycast(tf.position, tf.forward, out hit, speed))
        {
            // and if what we hit is NOT the player,
            if (!hit.collider.CompareTag("Player"))
            {
                //return false, meaning we can move forward
                return false;
            }
        }

        //otherwise, return true because we can move forward.
        return true;
    }


}
